package io.bowen.modules.oss.service.impl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.bowen.common.utils.PageUtils;
import io.bowen.common.utils.Query;
import io.bowen.modules.oss.dao.SysOssDao;
import io.bowen.modules.oss.entity.SysOssEntity;
import io.bowen.modules.oss.service.SysOssService;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service("sysOssService")
public class SysOssServiceImpl extends ServiceImpl<SysOssDao, SysOssEntity> implements SysOssService {

	@Override
	public PageUtils queryPage(Map<String, Object> params) {
		IPage<SysOssEntity> page = this.page(
			new Query<SysOssEntity>().getPage(params)
		);

		return new PageUtils(page);
	}
}
