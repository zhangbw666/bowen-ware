package io.bowen.modules.goods.dao;

import io.bowen.modules.goods.entity.GoodsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品管理
 * 
 * @author zhangbowen
 * @email 478231309@qq.com
 * @date 2020-06-19 11:03:25
 */
@Mapper
public interface GoodsDao extends BaseMapper<GoodsEntity> {
	
}
