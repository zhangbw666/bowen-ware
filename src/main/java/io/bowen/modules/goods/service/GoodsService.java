package io.bowen.modules.goods.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.bowen.common.utils.PageUtils;
import io.bowen.modules.goods.entity.GoodsEntity;

import java.util.Map;

/**
 * 商品管理
 *
 * @author zhangbowen
 * @email 478231309@qq.com
 * @date 2020-06-19 11:03:25
 */
public interface GoodsService extends IService<GoodsEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

