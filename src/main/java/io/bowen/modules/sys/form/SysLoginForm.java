package io.bowen.modules.sys.form;
import lombok.Data;

/**
 * 登录表单
 */
@Data
public class SysLoginForm {

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 验证码
     */
    private String captcha;

    /**
     * uuid
     */
    private String uuid;

}
