**项目说明** 

## bowen-ware
- bowen-ware是一个轻量级的，前后端分离的Java快速开发平台，能快速开发项目并交付。
- 支持MySQL、Oracle、SQL Server、PostgreSQL等主流数据库。
- 后台下载地址：https://gitee.com/zhangbw666/bowen-ware.git
- 前端下载地址：https://gitee.com/zhangbw666/bowen-admin.git
- 代码生成器：https://gitee.com/zhangbw666/bowen-generator.git

<br>
 
**项目特点** 
- 友好的代码结构及注释，便于阅读及二次开发。
- 实现前后端分离，通过token进行数据交互，前端再也不用关注后端技术。
- 灵活的权限控制，可控制到页面或按钮，满足绝大部分的权限需求。
- 页面交互使用Vue2.x，极大的提高了开发效率。
- 完善的代码生成机制，可在线生成entity、xml、dao、service、vue、sql代码，减少70%以上的开发任务。
- 引入quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能。
- 引入API模板，根据token作为登录令牌，极大的方便了APP接口开发。
- 引入Hibernate Validator校验框架，轻松实现后端校验。
- 引入云存储服务，已支持：七牛云、阿里云、腾讯云等。
- 引入swagger文档支持，方便编写API接口文档。

<br> 

**项目结构** 
```
bowen-ware
├─db  项目SQL语句
│
├─common 公共模块
│  ├─aspect 系统日志
│  ├─exception 异常处理
│  ├─validator 后台校验
│  └─xss XSS过滤
│ 
├─config 配置信息
│ 
├─modules 功能模块
│  ├─app API接口模块(APP调用)
│  ├─job 定时任务模块
│  ├─oss 文件服务模块
│  └─sys 权限模块
│ 
├─BowenApplication 项目启动类
│  
├──resources 
│  ├─mapper SQL对应的XML文件
│  └─static 静态资源
```

<br>

**技术选型** 
- 核心框架：Spring Boot 2.1
- 安全框架：Apache Shiro 1.4
- 视图框架：Spring MVC 5.0
- 持久层框架：MyBatis 3.3
- 定时器：Quartz 2.3
- 数据库连接池：Druid 1.0
- 日志管理：SLF4J1.7、Log4j
- 页面交互：Vue2.x 

<br> 

**后端部署**
- 通过git下载源码（git clone https://gitee.com/zhangbw666/bowen-ware.git）
- idea、eclipse需安装lombok插件，不然会提示找不到entity的get set方法。
- 创建数据库bowen_ware，数据库编码为UTF-8。
- 执行db/mysql.sql（默认使用mysql）文件，初始化数据。
- 修改application-dev.yml，更新MySQL账号和密码。
- Eclipse、IDEA运行BowenApplication.java，则可启动项目。
- Swagger文档路径：http://localhost:8082/bowen-ware/swagger/index.html
- Swagger注解路径：http://localhost:8082/bowen-ware/swagger-ui.html

<br> 

**前端部署**
- 本项目是前后端分离的，还需要部署前端，才能运行起来。
- 通过git下载源码（git clone https://gitee.com/zhangbw666/bowen-admin.git）
- 进入Idea Terminal终端：分别执行 npm install、npm run dev即可启动。
- 前端部署完毕，就可以访问项目了。URL：http://localhost:8082/#/login 账号：admin 密码：123456
 
<br>

**数据交互**
```
一般情况下，web项目都是通过session进行认证，每次请求数据时，都会把jsessionid放在cookie中，以便与服务端保持会话。  
本项目是前后端分离的，通过token进行认证（登录时，生成唯一的token凭证），每次请求数据时，都会把token放在header中，服务端
解析token，并确定用户身份及用户权限，数据通过json交互。
```
- 数据交互流程  
![数据交互流程](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/token.jpg?raw=true)

 **项目实战**
 ```
 完成一个商品的列表、添加、修改、删除功能，熟悉如何快速开发自己的业务功能模块。
 ```
 
- 步骤一 新建商品表tb_goods

 ```
CREATE TABLE `tb_goods` (
    `goods_id` bigint NOT NULL AUTO_INCREMENT,
    `name` varchar(50) COMMENT '商品名',
    `intro` varchar(500) COMMENT '介绍',
    `price` decimal(10,2) COMMENT '价格',
    `num` int COMMENT '数量',
    PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品管理';
 ```
 
 - 步骤二 生成代码
 
  ```
使用代码生成器生成代码，代码生成器工程见：https://gitee.com/zhangbw666/bowen-generator.git
  ```
  
 - 步骤三 执行脚本并运行
 
 ```
执行建表语句，创建tb_goods表，再启动bowen-generator项目(运行BowenApplication.java的main方法即可)  
我们只需勾选tb_goods，点击【生成代码】按钮，则可生成相应代码，如下所示：  
 ```
 ![执行生成代码](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-generator/code_auto.jpg?raw=true)  
 ![下载代码](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-generator/download.jpg?raw=true)
 
 - 步骤四 测试功能  
 
 1、先在bowen_ware库中，执行goods_menu.sql语句，这个SQL是生成菜单的，SQL语句如下所示：  
 
 ```
 -- 菜单SQL
 INSERT INTO `sys_menu` (`parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`)
     VALUES ('1', '商品管理', 'generator/goods', NULL, '1', 'config', '6');
 
 -- 按钮父菜单ID
 set @parentId = @@identity;
 
 -- 菜单对应按钮SQL
 INSERT INTO `sys_menu` (`parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`)
     SELECT @parentId, '查看', null, 'generator:goods:list,generator:goods:info', '2', null, '6';
 INSERT INTO `sys_menu` (`parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`)
     SELECT @parentId, '新增', null, 'generator:goods:save', '2', null, '6';
 INSERT INTO `sys_menu` (`parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`)
     SELECT @parentId, '修改', null, 'generator:goods:update', '2', null, '6';
 INSERT INTO `sys_menu` (`parent_id`, `name`, `url`, `perms`, `type`, `icon`, `order_num`)
     SELECT @parentId, '删除', null, 'generator:goods:delete', '2', null, '6';
```
 2、把生成的代码COPY到对应的模块  
 后台COPY  
 ![后台](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/goods-module-ware.jpg?raw=true)  
 前端COPY    
 ![前端](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/goods-module-admin.jpg?raw=true)  
 3、重启bowen-ware项目  
 ![页面1](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/goods-page1.jpg?raw=true)  
 ![页面2](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/goods-page2.jpg?raw=true)  
 ![页面3](https://ymtd-1307390667.cos.ap-guangzhou.myqcloud.com/img-bowen-ware/goods-page3.jpg?raw=true)  
 操作了以上几步，就把查询、新增、修改、删除就完成了，是不是很快。  
 
 
 
 
 